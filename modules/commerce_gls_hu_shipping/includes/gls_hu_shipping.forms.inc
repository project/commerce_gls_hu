<?php

/**
 * @file gls_hu_shipping.php
 * 
 * author: dj 
 * created: 2017.02.18. - 22:12:13
 * 
 * Description of gls_hu_shipping
 */

/**
 * Helper function to create select form.
 *
 * @param type $form
 * @param type $form_state
 * @param type $checkout_pane
 * @param type $order
 * @return string
 */
function _commerce_gls_hu_shipping_get_select_form(&$form, &$form_state) {
  if (empty($form_state['order'])) {
    return;
  }

  $order = $form_state['order'];
  $pane_form = array();

  //$form_state['checkout_pane'] = $checkout_pane['pane_id'];
  $lists = commerce_gls_hu_get_lists();

  $county = $city = $shop = 'none';
  $ppp_value = isset($order->gls_hu_field_gls[LANGUAGE_NONE][0]['value']) ? $order->gls_hu_field_gls[LANGUAGE_NONE][0]['value'] : NULL;


  if (!isset($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['county'])) {
    $county = isset($ppp_value) ? $lists['pp_list_reverse'][$ppp_value]['county'] : 'none';
    $city_options = _commerce_gls_hu_shipping_get_city_options($lists, $form_state, $county);
    $city = isset($ppp_value) ? $lists['pp_list_reverse'][$ppp_value]['city'] : 'none';
    $shop_options = _commerce_gls_hu_shipping_get_shop_options($lists, $form_state, $county, $city);
    $shop = isset($ppp_value) ? $ppp_value : 'none';
  }
  else {
    if (!empty($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['county'])) {
      $county = $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['county'];
    }
    else if (!empty($ppp_value)) {
      $county = $lists['pp_list_reverse'][$ppp_value]['county'];
    }
    $city_options = _commerce_gls_hu_shipping_get_city_options($lists, $form_state, $county);
    if ($county != 'none') {
      if (!empty($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['city'])) {
        $city = $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['city'];
      }
      else if (!empty($ppp_value)) {
        $city = $lists['pp_list_reverse'][$ppp_value]['city'];
      }
      $shop_options = _commerce_gls_hu_shipping_get_shop_options($lists, $form_state, $county, $city);
      if ($city != 'none') {
        if (!empty($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['shop'])) {
          $shop = $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['shop'];
        }
        else if (!empty($ppp_value)) {
          $shop = $ppp_value;
        }
      }
    }
  }

  $ajax = array(
    'callback' => 'commerce_gls_hu_shipping_choice_county_js',
    'wrapper' => 'ppp-datapicker-elements-wrapper',
  );

  $pane_form['county'] = array(
    '#type' => 'select',
    '#title' => t('County'),
    '#default_value' => $county,
    '#options' => $lists['pp_county'],
    '#ajax' => $ajax,
  );

  $pane_form['elements'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="ppp-datapicker-elements-wrapper">',
    '#suffix' => '</div>',
  );

  if (!isset($city_options[$city])) {
    $city = 'none';
  }
  $city_title = t('City');
  $city_options = _commerce_gls_hu_shipping_get_city_options($lists, $form_state);
  if (
      isset($form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['county']) &&
      trim($form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['county']) == 'Budapest'
  ) {
    $city_title = t('District');
    $city_options['none'] = t('Choose a district');
  }
  $pane_form['elements']['city'] = array(
    '#type' => 'select',
    '#title' => $city_title,
    '#default_value' => $city,
    '#options' => $city_options,
    '#ajax' => $ajax,
    '#disabled' => $county == 'none',
  );

  //$shop_options = _commerce_gls_hu_shipping_get_shop_options($lists, $form_state, $county, $city);
  if (!isset($shop_options[$shop]) || $city == 'none') {
    $shop = 'none';
  }
  $pane_form['elements']['shop'] = array(
    '#type' => 'select',
    '#title' => t('Shop'),
    '#default_value' => $shop,
    '#options' => $shop_options,
    '#required' => TRUE,
    '#ajax' => $ajax,
    '#disabled' => $city == 'none',
  );

  $shop_data = _commerce_gls_hu_shipping_get_shop_render($lists, $form_state, $shop);
  if (empty($shop_data) && !empty($lists['pp_rendered_shop'][$shop])) {
    $shop_data = $lists['pp_rendered_shop'][$shop];
  }
  $pane_form['elements']['shop_info'] = array(
    '#markup' => $shop_data,
  );

  $form['commerce_shipping']['commerce_gls_hu_shipping'] = $pane_form;

  array_unshift($form['buttons']['continue']['#validate'], '_commerce_gls_hu_pane_checkout_form_validate');
}

/**
 * Helper function to prepare map form.
 *
 * @param type $form
 * @param type $form_state
 * @param type $checkout_pane
 * @param type $order
 * @return string
 */
function _commerce_gls_hu_shipping_get_map_form(&$form, $form_state) {
  $pane_form = array();
  if (!isset($form_state['order'])) {
    return;
  }
  $order = $form_state['order'];

  // Define wrapper html with classes for form title.
  $title_classes = array(
    'shipping-title',
    'fieldset-title',
    'fieldset-legend',
  );
  $prefix = '<legend><span class="' . implode(" ", $title_classes) . '">';
  $suffix = '</span></legend>';

  // Define shipping service form.
  $state_hidden = '';
  if (isset($form_state['checkout_page']['page_id']) &&
      $form_state['checkout_page']['page_id'] !== 'checkout_page_gls_hu') {
    if (isset($form['commerce_shipping']['shipping_service']['#default_value'])) {
      $state_hidden = _commerce_gls_hu_is_gls_service($form['commerce_shipping']['shipping_service']['#default_value']) ? '' : 'hide';
    }
  }

  $pane_form['map'] = array(
    '#markup' => '',
    '#prefix' => '<div id="big-canvas" class="' . $state_hidden . '">',
    '#suffix' => '</div>',
  );

  $pane_form['gls_data'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  if (isset($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['store_data'])) {
    $shop_id = $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['store_data'];
  }
  if (empty($shop_id)) {
    $shop_id = isset($order->gls_hu_field_gls[LANGUAGE_NONE][0]['value']) ? $order->gls_hu_field_gls[LANGUAGE_NONE][0]['value'] : NULL;
    if (is_null($shop_id) && isset($order->data['gls_hu_shop'])) {
      $shop_id = $order->data['gls_hu_shop'];
    }
  }
  $pane_form['gls_data']['shop_id'] = array(
    '#type' => 'hidden',
    '#value' => $shop_id,
    '#attributes' => array(
      'id' => 'store-shop-id-value',
    ),
  );

  $shop_address = '';
  if (isset($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['store_address'])) {
    $shop_address = $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['store_address'];
  }
  if (empty($shop_address)) {
    if (isset($order->data['gls_data']['store_gmap_address'])) {
      $shop_address = $order->data['gls_data']['store_gmap_address'];
    }
  }
  $pane_form['gls_data']['store_gmap_address'] = array(
    '#type' => 'hidden',
    '#value' => $shop_address,
    '#attributes' => array(
      'id' => 'store-address-value',
    ),
  );

  $pane_form['gls_data']['pclshopid'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['pclshopid']) ? $order->data['gls_data']['pclshopid'] : '',
    '#attributes' => array(
      'id' => 'store-pclshopid-value',
    ),
  );

  $pane_form['gls_data']['name'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['name']) ? $order->data['gls_data']['name'] : '',
    '#attributes' => array(
      'id' => 'store-name-value',
    ),
  );

  $pane_form['gls_data']['ctrcode'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['ctrcode']) ? $order->data['gls_data']['ctrcode'] : '',
    '#attributes' => array(
      'id' => 'store-ctrcode-value',
    ),
  );

  $pane_form['gls_data']['zipcode'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['zipcode']) ? $order->data['gls_data']['zipcode'] : '',
    '#attributes' => array(
      'id' => 'store-zipcode-value',
    ),
  );

  $pane_form['gls_data']['city'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['city']) ? $order->data['gls_data']['city'] : '',
    '#attributes' => array(
      'id' => 'store-city-value',
    ),
  );

  $pane_form['gls_data']['address'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['address']) ? $order->data['gls_data']['address'] : '',
    '#attributes' => array(
      'id' => 'store-address-value',
    ),
  );

  $pane_form['gls_data']['contact'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['contact']) ? $order->data['gls_data']['contact'] : '',
    '#attributes' => array(
      'id' => 'store-contact-value',
    ),
  );

  $pane_form['gls_data']['phone'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['phone']) ? $order->data['gls_data']['phone'] : '',
    '#attributes' => array(
      'id' => 'store-phone-value',
    ),
  );

  $pane_form['gls_data']['email'] = array(
    '#type' => 'hidden',
    '#value' => !empty($order->data['gls_data']['email']) ? $order->data['gls_data']['email'] : '',
    '#attributes' => array(
      'id' => 'store-email-value',
    ),
  );


  if (!empty($state_hidden)) {
    $pane_form['#attributes'] = array(
      'class' => array($state_hidden),
    );
  }

  drupal_add_css('//online.gls-hungary.com/psmap/default.css', array('type' => 'external'));
  drupal_add_css(drupal_get_path('module', 'commerce_gls_hu_shipping') . '/css/commerce_gls_hu_bigcanvas.css', array('type' => 'file'));

  $form['commerce_shipping']['commerce_gls_hu_shipping'] = $pane_form;
  array_unshift($form['buttons']['continue']['#validate'], '_commerce_gls_hu_pane_checkout_form_validate');
}

/**
 * Helper function to prepare form alter.
 *
 * @param type $form
 * @param type $form_state
 */
function _commerce_gls_hu_shipping_form_alter(&$form, $form_state) {
  $copy_pane = _commerce_gls_hu_shipping_get_copy_elements($form_state['order']);
  if (isset($form['commerce_shipping']['commerce_gls_hu_shipping'])) {
    if (variable_get('gls_hu_form_type', 'select') === 'map') {
      $form['#attached']['js'][] = drupal_get_path('module', 'commerce_gls_hu_shipping') . '/js/commerce_gls_hu_jqinit.js';
      $form['#attached']['js'][] = '//maps.googleapis.com/maps/api/js?key=' . variable_get('gls_hu_gmap_api_key', '') . '&v=3';
      $form['#attached']['js'][] = '//online.gls-hungary.com/psmap/psmap.js';
      $form['#attached']['js'][] = drupal_get_path('module', 'commerce_gls_hu_shipping') . '/js/commerce_gls_hu_shipping.map_init.js';
      if (isset($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['store_address'])) {
        $shop_address = $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['store_address'];
      }
      if (empty($shop_address) && !empty($form_state['order']->data['gls_hu_shop_address'])) {
        $shop_address = $form_state['order']->data['gls_hu_shop_address'];
      }
      if (!empty($shop_address)) {
        $form['#attached']['js'][] = array(
          'data' => array('address' => $shop_address),
          'type' => 'setting'
        );
      }
    }
  }

  $form['commerce_shipping']['commerce_gls_hu_shipping']['#prefix'] = '<div id="checkout-pane-gls-hu-wrapper">';
  $form['commerce_shipping']['commerce_gls_hu_shipping']['#suffix'] = '</div>';
  $form['commerce_shipping']['shipping_service']['#ajax']['callback'] = 'commerce_gls_hu_shipping_pane_service_details_refresh';
  $form['commerce_shipping']['shipping_service']['#ajax']['wrapper'] = $form['#id'];

  /*
  if (isset($form['customer_profile_billing']) &&
      isset($form['customer_profile_shipping']) &&
      isset($form['commerce_shipping']['shipping_service']) &&
      isset($form['commerce_shipping']['commerce_gls_hu_shipping'])) {
    // Set gls hu ajax callback and wrapper of shipping service.
    if (!isset($form['customer_profile_billing']['#prefix'])) {
      $form['customer_profile_billing']['#prefix'] = '';
    }
    $form['customer_profile_billing']['#prefix'] .= '<div id="customer-profile-billing-gls-ajax-wrapper">';
    if (!isset($form['customer_profile_billing']['#suffix'])) {
      $form['customer_profile_billing']['#suffix'] = '';
    }
    $form['customer_profile_billing']['#suffix'] .= '</div>';

    if (isset($form_state['values']['commerce_shipping']['shipping_service'])) {
      $value = $form_state['values']['commerce_shipping']['shipping_service'];
    }
    else if (isset($_POST['commerce_shipping']['shipping_service'])) {
      $value = $_POST['commerce_shipping']['shipping_service'];
    }
    else if (isset($form_state['order']->data['shipping_service']->data['shipping_service']['name'])) {
      $value = $form_state['order']->data['shipping_service']->data['shipping_service']['name'];
    }

    if (isset($value) && _commerce_gls_hu_is_gls_service($value)) {
      // Disable billing copy and customer profile shipping if choosed gls point.
      if (isset($form['customer_profile_billing']['commerce_customer_profile_copy']) &&
          empty($form_state['values'][$copy_pane]['commerce_customer_profile_copy'])) {
        $form[$copy_pane]['commerce_customer_profile_copy']['#access'] = FALSE;
        $form[$copy_pane]['commerce_customer_profile_copy']['#default_value'] = FALSE;
        $element = _commerce_gls_hu_shipping_prepare_address_element($form_state['order'], $copy_pane, $form_state);
        _commerce_gls_hu_shipping_remove_clone($element['commerce_customer_address']['und'], $form[$copy_pane]['commerce_customer_address']['und']);
      }
      $form['customer_profile_shipping']['#title'] = '';
      if (!isset($form['customer_profile_shipping']['#prefix'])) {
        $form['customer_profile_shipping']['#prefix'] = '';
      }
      $form['customer_profile_shipping']['#prefix'] .= '<div id="customer-profile-shipping-gls-ajax-wrapper" class="hide">';
      if (!isset($form['customer_profile_shipping']['#suffix'])) {
        $form['customer_profile_shipping']['#suffix'] = '';
      }
      $form['customer_profile_shipping']['#suffix'] .= '</div>';

      if (isset($form['customer_profile_shipping']['profile_selection'])) {
        $form['customer_profile_shipping']['profile_selection']['#access'] = FALSE;
      }
      if (isset($form['customer_profile_shipping']['rendered_profile'])) {
        $form['customer_profile_shipping']['rendered_profile']['#access'] = FALSE;
      }
      if (isset($form['customer_profile_shipping']['edit_button'])) {
        $form['customer_profile_shipping']['edit_button']['#access'] = FALSE;
      }
      if (isset($form['customer_profile_shipping']['commerce_customer_address'])) {
        $form['customer_profile_shipping']['commerce_customer_address']['#access'] = FALSE;
      }
      if (isset($form['customer_profile_shipping']['cancel_button'])) {
        $form['customer_profile_shipping']['cancel_button']['#access'] = FALSE;
      }
    }
    else {
      if (isset($form['commerce_shipping']['commerce_gls_hu_shipping'])) {
        if (!isset($form['customer_profile_shipping']['#prefix'])) {
          $form['customer_profile_shipping']['#prefix'] = '';
        }
        $form['customer_profile_shipping']['#prefix'] .= '<div id="customer-profile-shipping-gls-ajax-wrapper">';
        if (!isset($form['customer_profile_shipping']['#suffix'])) {
          $form['customer_profile_shipping']['#suffix'] = '';
        }
        $form['customer_profile_shipping']['#suffix'] .= '</div>';
        if (isset($form['customer_profile_billing']['commerce_customer_profile_copy']) &&
            isset($value) && _commerce_gls_hu_is_gls_service($value)) {
          $form[$copy_pane]['commerce_customer_profile_copy']['#access'] = FALSE;
          $form[$copy_pane]['commerce_customer_profile_copy']['#default_value'] = FALSE;
        }
        $form['commerce_shipping']['commerce_gls_hu_shipping']['#title'] = '';
        if (variable_get('gls_hu_form_type', 'select') === 'select') {
          $form['commerce_shipping']['commerce_gls_hu_shipping']['county']['#access'] = FALSE;
          $form['commerce_shipping']['commerce_gls_hu_shipping']['elements']['#access'] = FALSE;
        }
        else {
          $form['commerce_shipping']['commerce_gls_hu_shipping']['title']['#markup'] = '';
          $form['commerce_shipping']['commerce_gls_hu_shipping']['store_data']['#access'] = FALSE;
        }
      }
    }
  }
   * 
   */

  //dsm($form);
}

/**
 * Helper function to get accessable address elements.
 *
 * @param type $order
 */
function _commerce_gls_hu_shipping_prepare_address_element($order, $copy_pane, $form_state) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $field_name = variable_get('commerce_' . $copy_pane . '_field', '');
  if (!empty($wrapper->{$field_name})) {
    $profile = $wrapper->{$field_name}->value();
    if (empty($profile)) {
      $profile_type = commerce_customer_profile_type_load('billing');
      $profile = commerce_customer_profile_new($profile_type['type']);
    }
    $element = array();
    field_attach_form('commerce_customer_profile', $profile, $element, $form_state);

    return $element;
  }

  return NULL;
}

/**
 * Helper functions to enable address field elements.
 *
 * @param array $elements
 * @param array $form
 */
function _commerce_gls_hu_shipping_remove_clone($elements, &$form) {
  $children = element_children($elements);
  foreach ($children as $key) {
    _commerce_gls_hu_shipping_remove_clone($elements[$key], $form[$key]);
  }
  if (isset($elements['#access'])) {
    $form['#access'] = $elements['#access'];
  }
  else {
    $form['#access'] = TRUE;
  }
}

/**
 * Helper for generating city options.
 */
function _commerce_gls_hu_shipping_get_city_options($lists, $form_state, $county = NULL) {
  if (empty($county) &&
      isset($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['county']) &&
      $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['county'] != 'none') {
    $county = $form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['county'];
  }
  if (!empty($lists['pp_city'][$county])) {
    $list['none'] = t('Choose a city');
    $cities = $lists['pp_city'][$county];
    if (is_array($cities)) {
      $list += $cities;
    }
    return $list;
  }

  return $lists['pp_all_cities'];
}

/**
 * Helper for generating shop options.
 */
function _commerce_gls_hu_shipping_get_shop_options($lists, $form_state, $county = NULL, $city = NULL) {
  if ((empty($county) || empty($city)) &&
      isset($form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['city']) &&
      $form_state['input']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['city'] != 'none') {
    $county = $form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['county'];
    $city = $form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['city'];
  }
  if (!empty($lists['pp_list'][$county][$city])) {
    $list['none'] = t('Choose a shop');
    $shops = $lists['pp_list'][$county][$city];
    if (is_array($shops)) {
      $list += $shops;
    }
    return $list;
  }

  return $lists['pp_all_shops'];
}

/**
 * Helper for display rendered shop data.
 */
function _commerce_gls_hu_shipping_get_shop_render($lists, $form_state, $shop = NULL) {
  if ($shop != 'none') {
    if (isset($form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['shop']) && $form_state['values'][$form_state['checkout_pane']]['elements']['shop'] != 'none') {
      $shop = $form_state['values']['commerce_shipping']['commerce_gls_hu_shipping']['elements']['shop'];
      if (!empty($lists['pp_rendered_shop'][$shop])) {
        return $lists['pp_rendered_shop'][$shop];
      }
    }
  }
  return '';
}
