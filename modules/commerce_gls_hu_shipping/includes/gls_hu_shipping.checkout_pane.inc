<?php

/**
 * @file
 * Define GLS point Pane to checkout process.
 */

/**
 * GLS point Pane: form callback.
 */
function commerce_gls_hu_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // if (commerce_gls_hu_shipping_is_gls_hu($order)) {
  // No GLS point shipping.
  // $referer = explode('/', $_SERVER['HTTP_REFERER']);
  // if (end($referer) == $form_state['checkout_page']['next_page']) {
  // if (!empty($form_state['checkout_page']['prev_page'])) {
  // $order = commerce_order_status_update($order, 'checkout_' . $form_state['checkout_page']['prev_page']);
  // drupal_goto(commerce_checkout_order_uri($order));
  // }
  // }
  // if (end($referer) == $form_state['checkout_page']['prev_page'] ||
  // $referer[3] == $form_state['checkout_page']['prev_page']) {
  // if (!empty($form_state['checkout_page']['next_page'])) {
  // $order = commerce_order_status_update($order, 'checkout_' . $form_state['checkout_page']['next_page']);
  // drupal_goto(commerce_checkout_order_uri($order));
  // }
  // }
  // }

  if (variable_get('gls_hu_form_type', 'select') == 'select') {
    $form = _commerce_gls_hu_get_select_form($form, $form_state, $checkout_pane, $order);
  }
  else {
    $form = _commerce_gls_hu_get_map_form($form, $form_state, $checkout_pane, $order);
  }

  return $form;
}

