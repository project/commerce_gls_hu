This is an implementation of hungarian shipping method GLS point for drupal
commerce shops.

The GLS point module read from automatic generated validboltlista.xml
and create data of shops.

The gls_hu_field is create a field for commerce order.
 
The gls_hu_shipping module is "clone" of commerce_flat_rate module.
Able to create GLS point shipping types and rates.

Installation:
- Copy the commerce GLS point folder to sites/all/modules

- Enable modules in admin/modules GLS point group with all dependencies

- Configure properly the commerce_shipping module.

- Go to admin/commerce/config/shipping page and choose
"add a GLS point service". You will see now same form as commerce_flat_rate
module, fill data and save. After save you can same rules condition adding to
this shipping mode.

- Add a product to cart and make the shopping procedure.
