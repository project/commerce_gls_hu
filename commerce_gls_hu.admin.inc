<?php

/**
 * @file
 * Administration form to GLS point modul.
 */

/**
 * Form builder function for module settings.
 */
function commerce_gls_hu_settings() {
  $form = array();

  $form['gls_hu_form_type'] = array(
    '#type' => 'select',
    '#title' => t('Form type'),
    '#description' => t('Select the form type.'),
    '#options' => array(
      'select' => t('Select', array(), array('context' => 'commerce_gls_hu')),
      'map' => t('Map', array(), array('context' => 'commerce_gls_hu')),
    ),
    '#default_value' => variable_get('gls_hu_form_type', 'select'),
    '#required' => TRUE,
  );

  $form['gls_hu_gmap_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('The Google map API key'),
    '#default_value' => variable_get('gls_hu_gmap_api_key', ''),
    '#description' => t("The google map api key.<br />Get a key from site: https://developers.google.com/maps/documentation/javascript/get-api-key"),
    '#states' => array(
      'visible' => array(
        ':input[name="gls_hu_form_type"]' => array('value' => 'map'),
      ),
    ),
  );

  $form['gls_hu_list_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The URL of GLS point point XML file'),
    '#default_value' => variable_get('gls_hu_list_url', 'http://online.gls-hungary.com/psmap/psmap_getdata.php?ctrcode=HU&action=getList&dropoff=0'),
    '#description' => t("Place of data from generating the list of GLS point.<br />Default is the original place from website GLS: http://online.gls-hungary.com/psmap/psmap_getdata.php?ctrcode=HU&action=getList&dropoff=0"),
    '#states' => array(
      'visible' => array(
        ':input[name="gls_hu_form_type"]' => array('value' => 'select'),
      ),
    ),
  );

  $form['button']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild GLS point list'),
    '#submit' => array('commerce_gls_hu_admin_list_reset_submit'),
    '#states' => array(
      'visible' => array(
        ':input[name="gls_hu_form_type"]' => array('value' => 'select'),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Rebuild GLS point point list manualy.
 */
function commerce_gls_hu_admin_list_reset_submit($form, &$form_state) {
  commerce_gls_hu_cron();
}
